package Day1;

public class StringCalculator {
	public StringCalculator() {
		
	}
	public int add (String numbers) {
		String delimiter = ",";
		//delimiter check
		if (numbers.length()==0) return 0;
		if (numbers.length()>2 && numbers.charAt(0)=='/' && numbers.charAt(1)=='/') {
			delimiter = numbers.substring(2, numbers.indexOf('\n'));
			numbers = numbers.substring(numbers.indexOf('\n')+1);
		}
		
		int returnValue=0;
		String regex ="("+ delimiter +")|(\n)";
		String[] numberArray= numbers.split(regex);
		for (int i=0; i<numberArray.length;i++) {
			returnValue+=Integer.parseInt(numberArray[i]);
		}
		return returnValue;
	}
}
