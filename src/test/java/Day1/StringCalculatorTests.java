package Day1;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

class StringCalculatorTests {
	@Test 
	void add0Numbers() {
	StringCalculator sc = new StringCalculator();
	assertEquals(0, sc.add(""));
	}
	@Test 
	void add1Numbers() {
	StringCalculator sc = new StringCalculator();
	assertEquals(2, sc.add("2"));
	}
	
	@Test 
	void addNumbers() {
	StringCalculator sc = new StringCalculator();
	assertEquals(5, sc.add("2,3"));
	}
	@Test 
	void addManyNumbers() {
	StringCalculator sc = new StringCalculator();
	assertEquals(15, sc.add("1,2,3,4,5"));
	}
	@Test 
	void handlesNewLine() {
	StringCalculator sc = new StringCalculator();
	assertEquals(5, sc.add("2\n3"));
	}
	@Test 
	void handlesDelimiters() {
	StringCalculator sc = new StringCalculator();
	assertEquals(5, sc.add("//;\n2;3"));
	}
	
}
